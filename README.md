# yo-angular-parse-template

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0. See github for reference to generator, ignore Ruby and compass instructions.

## Improvements

- Parse JS pre installed
- Replaced ngRoute with UI router
- Added modrewrite to grunt connect server to support html5 state routing
- Updated uglify to uglify-es to support es6 javascript
- Replaced compass sass compiler with libsass, drastically improving build times

## Notes

yo angular:route generator does not update app.js with generated state because ngRoute was replaced by ui router. State must be created manually.

## Build & development

Run `grunt build` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
